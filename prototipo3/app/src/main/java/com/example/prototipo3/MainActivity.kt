package com.example.prototipo3

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter

import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.content.DialogInterface

import android.content.Intent

import android.os.Bundle
import android.os.Looper
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import org.jetbrains.anko.toast
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import java.io.OutputStream
import java.util.*

var uuid: UUID = UUID.fromString("8989063a-c9af-463a-b3f1-f21d9b2b827b")

class MainActivity : AppCompatActivity() {


    lateinit var bluetoothAdapter: BluetoothAdapter


    val REQUEST_ENABLE_BLUETOOTH = 1

    companion object {
        val EXTRA_ADDRESS: String = "Devices_address"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        if (logIn()) {
            super.onCreate(savedInstanceState)

            var i = Intent(this, LogIn::class.java)
            startActivity(i)
            finish()
        } else {

            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
            val navView: BottomNavigationView = findViewById(R.id.nav_view)

            val navController = findNavController(R.id.nav_host_fragment)


            val appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.navigation_dashboard, R.id.navigation_home, R.id.navigation_notifications
                )
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if (bluetoothAdapter == null) {
                toast("El dispositivo no tiene bluetooth")
                return
            }
            if (!bluetoothAdapter.isEnabled) {
                val enBluInte = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enBluInte, REQUEST_ENABLE_BLUETOOTH)
            }




            try {

                lifecycleScope.launch {

                    escucharConexiones()
                }


            } catch (e: Exception) {

            }


        }
    }


    fun logIn(): Boolean {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        val gua = pref.getString("usuario", "No hay nada guardado")!!
        return (gua == "No hay nada guardado")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                if (bluetoothAdapter.isEnabled) {
                    toast("Bluetooth esta activado")
                } else {
                    toast("Bluetooth esta desactivado")
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                toast("Activación del bluettoth fue cancelada")
            }
        }


    }

    suspend fun escribir(s: String, outputStream: OutputStream) {
        withContext(Dispatchers.IO) {
            outputStream.write(('\n' + s).toByteArray())
            Log.i("hua", "Sending " + s)
            outputStream.flush()
            outputStream.write(('\n' + s).toByteArray())
            Log.i("hua", "Sending " + s)
        }

    }

    fun act(): Activity {
        return this
    }

    suspend fun escucharConexiones() {

        val serverSocket: BluetoothServerSocket?


        val btAdapter = BluetoothAdapter.getDefaultAdapter()
        if (btAdapter != null) {
            serverSocket = btAdapter.listenUsingRfcommWithServiceRecord("test", uuid) // 1

        } else {
            serverSocket = null

        }
        withContext(Dispatchers.IO) {
            var socket: BluetoothSocket
            Log.d("thread", "2-" + (Looper.myLooper() == Looper.getMainLooper()))

            try {
                socket = serverSocket!!.accept()  // 2
                if (socket != null) {
                    Log.i("server", "Connecting")
                    val inputStream = socket.inputStream
                    val outputStream = socket.outputStream
                    val id: String? = PreferenceManager.getDefaultSharedPreferences(act())
                        .getString("id", "No hay id")
                    try {
                        val sc = Scanner(inputStream)
                        val available = inputStream.available()
                        val bytes = ByteArray(available)
                        Log.i("hua", "Reading")

                        lateinit var ped: String
                        var i = 0
                        while (sc.hasNext()) {

                            Log.i("hua", "Message received")

                            var txt = sc.nextLine()
                            Log.i("hua", "Aqui va el mensaje---->" + txt)
                            if (txt.equals("ya")) {
                                break
                            }
                            if (i == 30) {
                                throw Exception()
                            }
                            i = i + 1
                        }
                        outputStream.write(("id:" + id!!).toByteArray())
                        Log.i("hua", "Sending")

                        outputStream.flush()


                        outputStream.write(('\n' + "ya").toByteArray())
                        Log.i("hua", "Sending")
                        outputStream.flush()
                        outputStream.write(('\n' + "ya").toByteArray())
                        Log.i("hua", "Sending")
                        outputStream.flush()

                        var txt = sc.nextLine()
                        Log.i("mensaje ya", "" + txt)

                        txt = sc.nextLine()
                        Log.i("mensaje pedido", "" + txt)

                        ped = sc.nextLine()
                        Log.i("ped", "" + ped)

                        var prestar = ""
                        val a = async { prestar=alert(ped) }
                        Log.i("hua", "await")
                        a.await()

                        outputStream.write(('\n' + prestar).toByteArray())
                        Log.i("hua", "Sending " + prestar)
                        outputStream.flush()
                        outputStream.write(('\n' + prestar).toByteArray())

                        dato(ped)
                        outputStream.flush()
                        sc.close()
                    } catch (e: Exception) {
                        Log.e("hua", "Cannot read data", e)
                    } finally {
                        inputStream.close()
                        outputStream.close()
                        socket.close()
                    }
                    //BluetoothServer(act(), socket).run()
                    withContext(Dispatchers.Main)
                    {
                        Log.d("thread", "3-" + (Looper.myLooper() == Looper.getMainLooper()))
                        Toast.makeText(act(), "prueba mesage", Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()

            }

        }

    escucharConexiones()
    }

    suspend fun alert(men: String): String {
        var prestar = "No"
        withContext(Dispatchers.Main) {

            val s = men.split("-")
            val builder = AlertDialog.Builder(act())
            builder.setTitle("Un papayamigo te necesita")
            builder.setMessage(s[0] + " quiere pedirte prestado: \n" + s[1] + " " + s[2] + " de " + s[3])


            builder.setPositiveButton("Prestar", DialogInterface.OnClickListener { dialog, which ->
                prestar = "Si"

            }
            )
            builder.setNegativeButton(
                "No prestar",
                DialogInterface.OnClickListener { dialog, which ->
                    prestar = "No"
                }
            )

            val dialog = builder.create()
            dialog.show()
        }
        delay(5000)
        return prestar

    }

    fun dato(ped: String) {
        var pref = PreferenceManager.getDefaultSharedPreferences(this)
        var debo = pref.getString("deben", "")
        if (debo.equals("")) {
            var x = debo.plus(ped)
            var ed = pref.edit()
            ed.putString("deben", x).apply()
        } else {
            var x = debo.plus(",").plus(ped)
            var ed = pref.edit()
            ed.putString("deben", x).apply()
        }
    }
    fun fin()
    {
        finish()
    }
}









