package com.example.prototipo3


import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.preference.PreferenceManager
import android.util.Log

import androidx.lifecycle.lifecycleScope

import kotlinx.android.synthetic.main.activity_gif_banana.*

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.toast
import java.util.*
import kotlin.NoSuchElementException

class GifBanana : AppCompatActivity() {

    lateinit var m_pairDevices: Set<BluetoothDevice>
    lateinit var ped:String
    private var siguiente = true
    val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gif_banana)

        bluetoothAdapter?.startDiscovery()
        bluetoothAdapter?.cancelDiscovery()
        pairedDevicedList()
       ped= (PreferenceManager.getDefaultSharedPreferences(this)).getString("momen","no hay").toString()

        /*      val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)

       registerReceiver(receiver, filter)
        Log.i("prueba","pasa por aca")
       var ped =  (PreferenceManager.getDefaultSharedPreferences(this)).getString("momen","no hay")
        t =BluetoothClient(m_pairDevices, this,ped)
        try {
            t.start()


        } catch (e: Exception) {
            Log.i("error", "error al conectar al disp" )
       }*/
        lifecycleScope.launch {
            conexiones()
        }
        cancelar.setOnClickListener {
            Log.i("thread", "parando el thread")
            siguiente = false
            finish()

        }
    }

    suspend fun conexiones() {
        withContext(Dispatchers.IO) {
            for (d in m_pairDevices) {
                procotolo(d)
                Log.i("valor sig", "" + siguiente)
                if (!siguiente) {
                    break
                }

            }
            if (siguiente) {
                fin()
            }
        }
    }
   suspend fun procotolo(a:BluetoothDevice)
    {

        var socket = a.createRfcommSocketToServiceRecord(uuid)
        var  outputStream = socket.outputStream
        var inputStream = socket.inputStream

        try {
            val sc = Scanner(inputStream)
            Log.i("client", "Connecting " + a.name)
            socket.connect()


            outputStream.write(('\n' + "hola").toByteArray())
            Log.i("xia", "Sending" + a.name)
            outputStream.flush()
            outputStream.write(('\n' + "ya").toByteArray())
            Log.i("xia", "Sending" + a.name)
            outputStream.flush()
            outputStream.write(('\n' + "ya").toByteArray())
            Log.i("xia", "Sending" + a.name)
            outputStream.flush()


            while (true) {
                Log.i("xia", "Message received")

                var txt = sc.nextLine()
                Log.i("xia", "Aqui va el mensaje---->" + txt)
                if (txt.equals("hola")) {

                }
                else if (txt.contains("id:")){
                    var arr=txt.split(":")
                    saveid(arr[1])
                    Log.i("xia", "id")
                }
                else if (txt.equals("ya")) {
                    break
                } else {
                    outputStream.close()
                    inputStream.close()
                    socket.close()
                }
            }

            outputStream.write(("" + '\n').toByteArray())
            Log.i("xia", "Sending" + a.name)
            outputStream.flush()

            outputStream.write(("sig pedido" + '\n').toByteArray())
            outputStream.flush()

            outputStream.write((ped)!!.toByteArray())
            Log.i("xia", "Sending pedido" + a.name + ped)

            outputStream.flush()

            outputStream.write(('\n'+"Pedido enviado").toByteArray())
            Log.i("xia", "Sending pedido confir")

            outputStream.flush()

            var txt = sc.nextLine()
            var i=0
            while (siguiente) {


                Log.i("xia", "Aqui va el mensaje---->" + txt)
                if (txt.equals("Si")) {
                    pantSig()
                    siguiente = false
                    save(a.name)
                    break
                }
                else if(txt.equals("No")){
                    siguiente = true
                    break
                }
                try {
                    txt = sc.nextLine()
                }
                catch (e:NoSuchElementException)
                {
                    i=i+1
                    detener()
                    if(i==5)
                    {
                        break
                    }
                }
            }

            sc.close()

        }
        catch(e: Exception) {

            Log.i("error","cae en excepcion" + a.name)
            e.printStackTrace()
            Log.i("stack ", e.stackTrace.toString())

        }
        catch(e:InterruptedException)
        {
            e.printStackTrace()
            siguiente=false
        }
        finally{
            outputStream.close()
            inputStream.close()
            socket.close()
        }
    }
    suspend fun detener()
    {
        delay(2000)
    }

    fun fin()
    {
       finish()
    }


    fun save(a:String)
    {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        var edit=pref.edit()
        edit.putString("nom",a).apply()

    }


    private fun pairedDevicedList(){
        m_pairDevices = bluetoothAdapter!!.bondedDevices
        val list: ArrayList<BluetoothDevice> = ArrayList()


        if(!m_pairDevices.isEmpty()) {
            for (device: BluetoothDevice in m_pairDevices) {
                list.add(device)

                Log.i("device ", "" + device)
            }
        }
        else
        {
            toast("no hay disspositivos cerca")
        }

        return
    }

    fun pantSig()
    {

        startActivity(Intent(this,Confirmacion::class.java))
        finish()
    }

    fun saveid(a:String)
    {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        var edit=pref.edit()
        edit.putString("presta",a).apply()
    }
}
