package com.example.prototipo3


import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

import kotlinx.android.synthetic.main.activity_log_in.*
import org.jetbrains.anko.toast
import java.lang.Exception


class LogIn : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var pref:SharedPreferences
    private val db = FirebaseFirestore.getInstance()
    private lateinit var dbRef: DocumentReference
    private var debo:String=""
    private var deben:String=""
    private var metd=false
    private lateinit var dbRef2:CollectionReference
    private lateinit var dbref: DocumentReference
    val REQUEST_ENABLE_BLUETOOTH = 1
    var direciones:String =""
    lateinit var bluetoothAdapter: BluetoothAdapter




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth = FirebaseAuth.getInstance()
        dbRef2=db.collection("usuarios")

        setContentView(R.layout.activity_log_in)
        pref= PreferenceManager.getDefaultSharedPreferences(this)


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            toast("El dispositivo no tiene bluetooth")
            return
        }
        if (!bluetoothAdapter.isEnabled) {
            val enBluInte = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enBluInte, REQUEST_ENABLE_BLUETOOTH)
        }


        nombre.visibility=View.GONE
        local.visibility=View.GONE


        checkCon()

        btnIngresar.setOnClickListener()
        {
            if(metd)
            {
                crear()
            }
            else {
                iniciar()
            }
        }

        crearCuenta.setOnClickListener()
        {
            correo.text.clear()
            textContra.text.clear()
            nombre.text.clear()
            metd=!metd
            if(metd) {
                nombre.visibility=View.VISIBLE
                local.visibility=View.VISIBLE
                btnIngresar.text = "Crear Cuenta"
                crearCuenta.text = "Ya tengo cuenta"

            }
            else
            {
                nombre.visibility=View.GONE
                local.visibility=View.GONE
                btnIngresar.text = "Ingresa"
                crearCuenta.text = "Soy nuevo, crear una cuenta"

            }

        }

    }
    fun crear(){

        if((!(nombre.text.toString()).equals(""))&& !((correo.text.toString()).equals(""))&& !((textContra.text.toString()).equals("")) && !((local.text.toString()).equals("")))
        {
            if(checkCon())
            {

                auth.createUserWithEmailAndPassword(correo.text.toString(), textContra.text.toString())
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {


                            Toast.makeText(
                                this,
                                "Se creo correctamente el usuario",
                                Toast.LENGTH_LONG
                            ).show()
                            val userp = hashMapOf(
                                "nombre" to nombre.text.toString(),
                                "local" to local.text.toString(),
                                "email" to correo.text.toString(),
                                "ID" to task.result!!.user!!.uid

                            )
                            dbRef2.add(userp)
                            var i = Intent(this, MainActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            Toast.makeText(this, "Error al crear usuario", Toast.LENGTH_LONG).show()

                        }

                    }
            }
            else
            {
                progressBar3.visibility=View.GONE
                showAlertInternet()

            }
        }
        else
        {
            Log.i("else","cae en el if")
        }
    }

    fun iniciar()
    {

        progressBar3.visibility=View.VISIBLE
        if(checkCon()){
            if (correo.text.equals("") || textContra.text.toString().equals(""))
            {
                progressBar3.visibility=View.GONE
                Toast.makeText(
                    this,
                    "Completen los campos de correo y contraseña",
                    Toast.LENGTH_LONG
                ).show()
            }
            else
            {

                btnIngresar.isEnabled = false
                crearCuenta.isEnabled=false


                auth.signInWithEmailAndPassword(correo.text.toString(), textContra.text.toString())
                    .addOnCompleteListener(this)
                    { task ->
                        if (task.isSuccessful) {

                            save(correo.text.toString(),
                                textContra.text.toString(),
                                task.result!!.user!!.uid)

                            db.collection("usuarios").get()
                                .addOnSuccessListener { documents ->
                                    for (document in documents) {
                                        if (((document.data.get("ID"))!!.equals(task.result!!.user!!.uid)))
                                        {
                                            dbRef = document.reference
                                        }
                                    }

                                    db.collection("prestamos").get().addOnSuccessListener { documents ->
                                            for (document in documents) {
                                                if (((document.data.get("recibe").toString()).equals(dbRef.toString()))
                                                ) {
                                                    try{
                                                    dbref =
                                                        document.get("presta") as DocumentReference

                                                        dbref.get()
                                                            .addOnSuccessListener { correo ->

                                                                debo = debo + correo.data!!.get("nombre") + "-" + document.get("cantidad").toString() + "-" + document.get("medida").toString() + "-" + document.get("producto").toString() +"-"+document.get("estado").toString()+","
                                                                pref.edit().putString("debo", debo).apply()
                                                                direciones=direciones+document.id+","
                                                                pref.edit().putString("direccion", direciones).apply()

                                                            }
                                                    }
                                                    catch (e: Exception)
                                                    {
                                                        pref.edit().putString("debo", debo).apply()
                                                    }
                                                }
                                            }
                                        }
                                }
                                .addOnFailureListener { exception ->
                                    Log.i("error", "Error getting documents: ", exception)

                                }
                            db.collection("usuarios").get().addOnSuccessListener{ documents ->
                                    for (document in documents) {
                                        if (((document.data.get("ID"))!!.equals(task.result!!.user!!.uid)))
                                        {
                                            dbRef=document.reference
                                        }
                                    }

                                    db.collection("prestamos").get()
                                        .addOnSuccessListener { documents ->
                                            for (document in documents) {

                                                if (
                                                    ((document.data.get("presta").toString()).equals(dbRef.toString()))
                                                )
                                                {
                                                    try{

                                                    dbref =
                                                        document.get("recibe") as DocumentReference


                                                    dbref.get().addOnSuccessListener { correo ->
                                                        deben =
                                                            deben + correo.data!!.get("nombre")
                                                                .toString() + "-" + document.get(
                                                                "cantidad"
                                                            )
                                                                .toString() + "-" + document.get("medida")
                                                                .toString() + "-" + document.get("producto")
                                                                .toString() + "-" + document.get("estado")
                                                                .toString()+","
                                                        pref.edit().putString("deben", deben)
                                                            .apply()

                                                    }
                                                }
                                                    catch(e:Exception)
                                                    {
                                                        pref.edit().putString("deben", deben)
                                                            .apply()
                                                    }
                                                }
                                            }
                                        }
                                        .addOnCompleteListener{
                                            var i = Intent(this, MainActivity::class.java)
                                            startActivity(i)
                                            finish()}
                                }
                        }
                        else
                        {
                            showAlert()
                            btnIngresar.isEnabled = true
                            crearCuenta.isEnabled = true
                            progressBar3.visibility=View.GONE
                        }


                    }
            }
        }
        else
        {
            btnIngresar.isEnabled = true
            showAlertInternet()
            progressBar3.visibility=View.GONE
        }
    }

    fun checkCon():Boolean{
        val cm=getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ac=cm.activeNetworkInfo
        val isC= ac!=null && ac.isConnectedOrConnecting
        return isC
    }



    fun save(usu:String,cont:String,id:String)
    {
        var edit=pref.edit()
        edit.putString("usuario",usu).apply()
        edit.putString("contr",cont).apply()
        edit.putString("id",id).apply()
        Log.i("id",id)
    }
    fun showAlert()
    {
        progressBar3.visibility=View.GONE
        Toast.makeText(this,"Usuario o contraseña incorrecta",Toast.LENGTH_LONG).show()

    }
    fun showAlertInternet()
    {

        Toast.makeText(this,"No hay conexion a internet en este momento",Toast.LENGTH_LONG).show()
    }
}
