package com.example.prototipo3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_calificacion.*
import org.jetbrains.anko.toast
import java.lang.Exception

class calificacion : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calificacion)
        button.setOnClickListener()
        {
            try{
            if ( (calificacion.text.toString().equals(""))||(calificacion.text.toString()).toInt() < 1 || (calificacion.text.toString()).toInt() > 5) {
                Toast.makeText(
                    this,
                    "Ingresa valores entre 1 y 5 en la calificacion",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                save(calificacion.text.toString())
                val i = Intent(this, SolicitudExitosa::class.java)

                startActivity(i)
                finish()
            }
        }
            catch (e:Exception)
            {
                Toast.makeText(this, "Ingrese una calificacion",Toast.LENGTH_LONG)
            }
        }
    }

    fun save(cal:String)
    {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        var edit = pref.edit()
        edit.putString("calificacion", cal).apply()
    }

    override fun onBackPressed() {

        Toast.makeText(this,"No puedes retroceder, por favor termina de calificar",Toast.LENGTH_LONG).show()
    }


}
