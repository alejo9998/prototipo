package com.example.prototipo3.ui.dashboard

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface

import android.content.SharedPreferences

import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.prototipo3.MainActivity
import com.example.prototipo3.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore


class DashboardFragment : Fragment() {


    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()
    lateinit var dbRef: DocumentReference
    lateinit var pref: SharedPreferences
    lateinit var  acti: Activity
    lateinit var direcciones:String
    lateinit var correo:String
    lateinit  var contra:String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)

        dashboardViewModel.text.observe(this, Observer {})

        

        auth = FirebaseAuth.getInstance()

        acti=(activity as MainActivity).act()


        var pref= PreferenceManager.getDefaultSharedPreferences(root.context)
        var list: ListView =root.findViewById(R.id.listDebo)
        correo = pref.getString("usuario", "No hay").toString()

        contra= pref.getString("contr", "No hay").toString()

        var el=pref.getString("debo","")

        direcciones=pref.getString("direccion","").toString()

        if(el.equals(""))
        {
            el="No le debes a nadie"
        }
        var pres=el!!.split(",")


        var a = ArrayAdapter(
                root.context,
                android.R.layout.simple_selectable_list_item,
                pres
         )
        var d = direcciones.split(",")

        list.adapter = a
        list.setOnItemClickListener( AdapterView.OnItemClickListener{parent, view, position, id ->

            val builder = AlertDialog.Builder(acti)
            builder.setTitle("Pagar un favor")
            builder.setMessage("¿Deseas marcar como pagado el favor de "+ (a.getItem(position)!!.split("-")).get(0)+"?")


            builder.setPositiveButton("Pagar", DialogInterface.OnClickListener { dialog, which ->

                pagar(d.get(position))

            }
            )
            builder.setNegativeButton(
                "No pagar",
                DialogInterface.OnClickListener { dialog, which ->

                }
            )

            val dialog = builder.create()
            dialog.show()
        })
        return root
    }


    fun pagar(prestamo:String)
    {
        auth.signInWithEmailAndPassword(correo.toString(), contra.toString())
            .addOnCompleteListener(acti){ task ->

                db.collection("prestamos").document(prestamo).update("estado","Pagado")
            }

    }

}